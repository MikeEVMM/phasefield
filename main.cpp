#include <cstdio>
#include <cmath>
#include <iostream>
#include <fstream>

// Parameters
const double lattice_spacing = 0.01;
const double epsilon = 3*lattice_spacing; // Interface width
const int lattice_size_h = 200;
const int lattice_size_v = 200;
const double M = 1; // Constant in Cahn-Hillard eq.

const double dt = 1e-10; // In seconds
const double sim_time = 10; // In seconds
const int print_every_steps = 500;

const double nucleus_value = 0.8164966; // Approx sqrt(2/3)
const double cell_value = 0;
const double not_cell_value = -0.8164966; // Approx -sqrt(2/3)

const int cell_size = lattice_size_h / 4;

// Function declarations
void init();
void step();
void clean();
void print(std::string filename, int h_size, int v_size, double* map);

double functional_integral(int i, int j, double phi); // Get value of integral in functional derivative @ (i, j)
void update_functional();

double initial_condition(int i, int j);
double laplacian(int i, int j, int h_size, double* lattice);

int bound_i(int i);
int bound_j(int j);

inline int indexOf(int i, int j);

// Variables
double* working_lattice; // Phase field lattice (working) (2D collapsed)
double* prev_lattice; // Phase field lattice (last step) (2D collapsed)
double* functional_lattice; // Lattice with values of energy functional derivative (last step) (2D collapsed)

// Body

int main() {
    std::cout << "Initializing" << std::endl;
    init();
    std::cout << "Initialized" << std::endl;

    std::cout << "Running" << std::endl;

    char name_buffer [30];
    print("Start.dat", lattice_size_h, lattice_size_v, prev_lattice);

    int total_steps = sim_time / dt;
    for (int stp = 0; stp < total_steps; ++stp) {
        step();

        if (stp % print_every_steps == 0) {
            sprintf(name_buffer, "%d.dat", stp);
            print(name_buffer, lattice_size_h, lattice_size_v, prev_lattice);
        }
    }

    print("End.dat", lattice_size_h, lattice_size_v, prev_lattice);

    std::cout << "Done! Cleaning" << std::endl;

    clean();

    return 0;
}

void init () {
    // Initialize lattices
    working_lattice = new double[lattice_size_h * lattice_size_v];
    prev_lattice = new double[lattice_size_h * lattice_size_v];
    functional_lattice = new double[lattice_size_h * lattice_size_v];

    // Populate
    for (int i = 0; i < lattice_size_h; ++i) {
        for (int j = 0; j < lattice_size_v; ++j) {
            prev_lattice[indexOf(i,j)] = initial_condition(i, j);
        }
    }

    update_functional();
}

void step () {
    for (int j = 0; j < lattice_size_h; ++j) {
        for (int i = 0; i < lattice_size_v; ++i) {
            working_lattice[indexOf(i, j)] = prev_lattice[indexOf(i ,j)] +
                dt * M * laplacian(i, j, lattice_size_h, functional_lattice);
        }
    }

    prev_lattice = working_lattice;

    update_functional();
}

void clean () {
    delete [] working_lattice;
    delete [] prev_lattice;
    delete [] functional_lattice;
}

void print(std::string filename, int h_size, int v_size, double* map) {
    std::cout << "Printing " << filename << std::endl;

    std::ofstream file;
    file.open(filename.c_str());

    if (!file.is_open())
        std::cout << "Cannot write?" << std::endl;

    for (int j = 0; j < v_size; ++j) {
        for (int i = 0; i < h_size; ++i) {
            file << map[i + j * h_size] << " ";
        }
        file << "\n";
    }

    file.close();

    std::cout << "Done printing " << filename << std::endl;
}

void update_functional () {
    double phi;
    for (int j = 0; j < lattice_size_v; ++j) {
        for (int i = 0; i < lattice_size_h; ++i) {
            phi = prev_lattice[indexOf(i, j)];
            functional_lattice[indexOf(i, j)] = (
                    2 * phi - 4 * phi * phi * phi + 1.5 * phi * phi * phi * phi * phi
                    - epsilon * laplacian(i, j, lattice_size_h, prev_lattice) +
                    functional_integral(i, j, phi)
            );
        }
    }
}

double functional_integral (int i, int j, double phi) {
    double other_phi;
    double sum = 0;
    for (int newJ = 0; newJ < lattice_size_v; ++newJ) {
        for (int newI = 0; newI < lattice_size_h; ++newI) {
            other_phi = prev_lattice[indexOf(newI, newJ)];
            sum += 2 * (phi - other_phi);
        }
    }
    return sum;
}

double initial_condition (int i, int j) {
    int x = i - 0.5*lattice_size_h;
    int y = j - 0.5*lattice_size_v;
    int r_sqr = x*x + y*y;

    if (r_sqr < cell_size*cell_size/4) {
        if (r_sqr < cell_size*cell_size/16)
            return nucleus_value;
        return cell_value;
    }
    return  not_cell_value;
}

double laplacian (int i, int j, int h_size, double* lattice) {
    return (
            lattice[bound_i(i - 1) + j*h_size] +
            lattice[bound_i(i + 1) + j*h_size] +
            lattice[i + bound_j(j - 1)*h_size] +
            lattice[i + bound_j(j + 1)*h_size] -
            4*lattice[i + j*h_size]
    ) / (lattice_spacing*lattice_spacing);
}

int bound_i (int i) {
    return (i + lattice_size_h) % lattice_size_h;
}

int bound_j (int j) {
    return (j + lattice_size_v) % lattice_size_v;
}

inline int indexOf(int i, int j) {
    return  i + j * lattice_size_h;
}
